import discord
import random
import re

import settings

# 接続に必要なオブジェクトを生成
client = discord.Client()

# 起動時に動作する処理
@client.event
async def on_ready():
    # 起動したらターミナルにログイン通知が表示される
    print('logged in to Discord')

# メッセージ受信時に動作する処理
@client.event
async def on_message(message):
    # メッセージ送信者がBotだった場合は無視する
    if message.author.bot:
        return

    if client.user in message.mentions:
        mention_pattern = re.compile(fr'<@{client.user.id}>.*')
        match = mention_pattern.match(message.content)
        if match:
            # Botに対しての発言

            # 発言からmentionを削除
            mod_content = message.content
            mod_content = mod_content.lstrip(f'<@{client.user.id}> ')

             # hello
            if mod_content == 'hello':
                await message.channel.send(f'{message.author.mention} Hello')

            # food
            food_pattern = re.compile('food (.+[,|、].+)')
            match = food_pattern.match(mod_content)
            if match:
                foods = match.group(0).lstrip('food ')
                result = random.choice([i for i in re.split(r'[,、]',foods) if i != ''])
                await message.channel.send(f'{message.author.mention} {result}')
            if mod_content == 'food':
                food_data = open("food.txt", "r")
                lines = food_data.readlines()
                random.seed()
                await message.channel.send(f'{message.author.mention} {random.choice(lines)}')

# Botの起動とDiscordサーバーへの接続
client.run(settings.DISCORD_ACCESS_TOKEN)
